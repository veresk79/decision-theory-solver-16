\begin{align*}
&v_{i}\left(\textbf{h}_{i}, w\right ) = \sum_{j}p_{j}\left ( \textbf{h}_{i} \right )\left[\left ( r_{j}\left ( \textbf{h}_{i} \right ) +ws_{j}\left ( \textbf{h}_{i} \right )\right )+v_{k}\left ( \textbf{h}_{k}, w \right )\right],
i\in C, j\in C\left ( i|\textbf{h}_{i} \right ) \\
\end{align*}
<html> 
	<h2 align='center'>  Формальная постановка задачи  </h2>  
	<p>     
		Множество решений D = {d<sub>0</sub>, d<sub>1</sub>, d<sub>2</sub>, d<sub>3</sub>}, 
		где d<sub>i</sub> - решение выпускать i батарей (i = 0, 1, 2, 3). <br/>

		Случайная величина Y = {y<sub>0</sub>, y<sub>1</sub>, y<sub>2</sub>, y<sub>3</sub>} описывает спрос на батареи, 
		y<sub>i</sub> - спрос на i батарей (i = 0, 1, 2, 3).<br/>

		Принятое решение описывается величиной <i>D</i>, принимающей значения из множества D. 
		Вероятность события y<sub>i</sub> обозначит как p<sub>i</sub> = Pr{Y ≤ i}, <br/>
		функция распределения случайной величины Y: P(i) = Pr{Y ≤ i}.
	</p>    
	<p>     
		Каждому из возможных исходов (таковых будет (n + 1)<sup>2</sup> = 16, т. к. n = 3) поставим в соответствие ценность исхода <br/>
		V(y, d) = min{y, d}v<sub>s</sub> - dv<sub>c</sub>.<br/>
	</p>   
	<p>     
		Полезность.<br/>
		Полезность решений рассчитывается как:<br/>
		u(d<sub>0</sub>) = E<sub>Y</sub>[V(y, 0)] = 0;<br/>

		u(d<sub>1</sub>) = E<sub>Y</sub>[V(y, 1)] = (v<sub>s</sub> - v<sub>c</sub>) - p<sub>0</sub>v<sub>s</sub>;<br/>

		u(d<sub>2</sub>) = E<sub>Y</sub>[V(y, 2)] = 2(v<sub>s</sub> - v<sub>c</sub>) - 2p<sub>0</sub>v<sub>s</sub> - 
		p<sub>1</sub>v<sub>s</sub>;<br/>

		u(d<sub>3</sub>) = E<sub>Y</sub>[V(y, 3)] = 3(v<sub>s</sub> - v<sub>c</sub>) - 3p<sub>0</sub>v<sub>s</sub> - 
		2p<sub>1</sub>v<sub>s</sub> - p<sub>2</sub>v<sub>s</sub>;<br/><br/>
		Выбор решения.<br/>Байесовский подход в данной задаче предполагает, что лучшее решение максимизирует полезность:<br/>
		d<sup>*</sup> = argmax E<sub>Y</sub>[V(y, d)] = argmax u(d), d &#8712 D.<br/><br/>
	</p>    
	<p>Дерево решений будет иметь следующий вид:<br/><br/></p>  
</html>
package ru.spb.beavers.modules.coach_desision_of_stratagy_game.gui;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.*;
import java.awt.*;

import ru.spb.beavers.modules.coach_desision_of_stratagy_game.gui.rabsourse.ElementKord;
import ru.spb.beavers.modules.coach_desision_of_stratagy_game.gui.rabsourse.GroupableTableHeader;
import ru.spb.beavers.modules.coach_desision_of_stratagy_game.gui.rabsourse.datainp;
import ru.spb.beavers.modules.coach_desision_of_stratagy_game.gui.rabsourse.PanelEl;

/**
 * Created by Sasha on 09.04.2015.
 */
public class InputPanel {

    private int r;
    private JPanel extra_option_panel;
    private JPanel pnn;
    private JTable table;
    //private Node root_node;
   // private ConflictSolver solver;
    private ElementKord last_el_coords;
    datainp dt;
    boolean kr;

    ExamplePanel epb;
    DefaultTableModel dm = new DefaultTableModel(){
        @Override
        public boolean isCellEditable(int row, int column) {
            return (column == 5) ? false : true;
        }
    };
    public InputPanel(JPanel panel, datainp rs, ExamplePanel ep)
    {

        dt=rs;
        epb=ep;
        last_el_coords = new ElementKord();
        pnn=panel;
        panel.setLayout(null);
        panel.setSize(760, 1120);
        panel.setPreferredSize(new Dimension(750, 1120));

        createInputDataTable(panel);
      //  createOptionPanel(panel);

        //root_node = getTree();
      //  treeInit(root);
      //  solver = getSolver();

    }

    public int getr()
    {
        return r;
    }

    public void  ubdpanel(ExamplePanel epb, JPanel panel)
    {
        epb=new ExamplePanel(panel, dt);

    }


    private void createInputDataTable(JPanel panel)
    {
        PanelEl.setTextHeader("Введите вероятности и рейтинги:", 20, 400, 20, last_el_coords, panel);


       setdatatable();
        table = new JTable( dm ) {
            DefaultTableCellRenderer renderRight = new DefaultTableCellRenderer();

            {
                renderRight.setHorizontalAlignment(SwingConstants.CENTER);

            }

            @Override
            public TableCellRenderer getCellRenderer (int arg0, int arg1) {
                return renderRight;
            }

            protected JTableHeader createDefaultTableHeader() {
                return new GroupableTableHeader(columnModel);
            }
        };
        table.getModel().addTableModelListener(new TableModelListener() {


            @Override
            public void tableChanged(TableModelEvent e) {
                if ((e.getType() == TableModelEvent.UPDATE)||(!kr)) {
                   // Node node = root_node.getChildRecursively(table.getValueAt(e.getFirstRow(), 0).toString());
                    if (dt == null)
                        throw new RuntimeException("Runtime: " + table.getValueAt(e.getFirstRow(), 0).toString());
                    switch (e.getColumn()) {
                        case 0:
                            dt.ubdPg(Double.parseDouble(table.getModel().getValueAt(e.getFirstRow(), e.getColumn()).toString()), e.getFirstRow());

                            //epb.settable(); // ubdpanel(epb, epb.getPanel());
                            break;
                        case 1:
                            dt.ubdPt(Double.parseDouble(table.getModel().getValueAt(e.getFirstRow(), e.getColumn()).toString()), e.getFirstRow());
                           // ubdpanel(epb, epb.getPanel());
                           // epb.settable();

                            break;
                        case 2:
                            dt.ubdRwin( Integer.valueOf(table.getModel().getValueAt(e.getFirstRow(), e.getColumn()).toString()), e.getFirstRow() );
                           // ubdpanel(epb, epb.getPanel());
                           // epb.settable();
                            break;
                        case 3:
                            dt.ubdRlose( Integer.valueOf(table.getModel().getValueAt(e.getFirstRow(), e.getColumn()).toString()), e.getFirstRow() );
                           // ubdpanel(epb, epb.getPanel());
                           // epb.settable();
                            break;
                        case 4:
                            dt.ubdRdraw( Integer.valueOf(table.getModel().getValueAt(e.getFirstRow(), e.getColumn()).toString()), e.getFirstRow() );
                           // ubdpanel(epb, epb.getPanel());
                            //epb.settable();
                            break;
                            default:
                            throw new RuntimeException("Че-то пошло не так");
                    }
                }
            }
        });
        TableColumnModel cm = table.getColumnModel();

        table.setPreferredSize(new Dimension(600, table.getRowHeight() * 10));
        table.setSize(new Dimension(600, table.getRowHeight() * 10 + 60));

        JScrollPane scroll = new JScrollPane(table);
        scroll.setSize(new Dimension(600, 278));
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        last_el_coords.updateValues(10, scroll.getHeight());
        scroll.setLocation(panel.getWidth() / 2 - table.getWidth() / 2, last_el_coords.y);
        panel.add(scroll);
    }
    public JPanel getpanel()
    {
        return pnn;
    }
    public datainp fetData() {return  dt;}
    public void setdatatable()
    {
        dm.setDataVector(new Object[][]{
                        {String.valueOf(dt.getpg(0)), String.valueOf(dt.getpt(0)), String.valueOf(dt.getrwin(0)), String.valueOf(dt.getrlose(0)), String.valueOf(dt.getrdraw(0))},
                        {String.valueOf(dt.getpg(1)), String.valueOf(dt.getpt(1)), String.valueOf(dt.getrwin(1)), String.valueOf(dt.getrlose(1)), String.valueOf(dt.getrdraw(1))},
                        {String.valueOf(dt.getpg(2)), String.valueOf(dt.getpt(2)), String.valueOf(dt.getrwin(2)), String.valueOf(dt.getrlose(2)), String.valueOf(dt.getrdraw(2))},
                        {String.valueOf(dt.getpg(3)), String.valueOf(dt.getpt(3)), String.valueOf(dt.getrwin(3)), String.valueOf(dt.getrlose(3)), String.valueOf(dt.getrdraw(3))},
                        {String.valueOf(dt.getpg(3)), String.valueOf(dt.getpt(3)), String.valueOf(dt.getrwin(3)), String.valueOf(dt.getrlose(3)), String.valueOf(dt.getrdraw(3))},
                        {String.valueOf(dt.getpg(4)), String.valueOf(dt.getpt(4)), String.valueOf(dt.getrwin(4)), String.valueOf(dt.getrlose(4)), String.valueOf(dt.getrdraw(4))},
                        {String.valueOf(dt.getpg(5)), String.valueOf(dt.getpt(5)), String.valueOf(dt.getrwin(5)), String.valueOf(dt.getrlose(5)), String.valueOf(dt.getrdraw(5))},
                        {String.valueOf(dt.getpg(6)), String.valueOf(dt.getpt(6)), String.valueOf(dt.getrwin(6)), String.valueOf(dt.getrlose(6)), String.valueOf(dt.getrdraw(6))},
                        {String.valueOf(dt.getpg(7)), String.valueOf(dt.getpt(7)), String.valueOf(dt.getrwin(7)), String.valueOf(dt.getrlose(7)), String.valueOf(dt.getrdraw(7))},
                        {String.valueOf(dt.getpg(8)), String.valueOf(dt.getpt(8)), String.valueOf(dt.getrwin(8)), String.valueOf(dt.getrlose(8)), String.valueOf(dt.getrdraw(8))},
                        {String.valueOf(dt.getpg(9)), String.valueOf(dt.getpt(9)), String.valueOf(dt.getrwin(9)), String.valueOf(dt.getrlose(9)), String.valueOf(dt.getrdraw(9))},

                },
                new Object[]{"Веротность\n забить гол", "Вероятность\n забить тачдаун", "Рейтинг в случае\nпобеды", "Рейтинг в случае\n парожаения", "Рейтинг в случае\n ничьи"});

    }


}

package ru.spb.beavers.modules;

import info.monitorenter.gui.chart.Chart2D;
import info.monitorenter.gui.chart.IAxis;
import info.monitorenter.gui.chart.IAxisScalePolicy;
import info.monitorenter.gui.chart.ITrace2D;
import info.monitorenter.gui.chart.axis.scalepolicy.AxisScalePolicyManualTicks;
import info.monitorenter.gui.chart.rangepolicies.RangePolicyFixedViewport;
import info.monitorenter.gui.chart.traces.Trace2DSimple;
import info.monitorenter.util.Range;
import org.scilab.forge.jlatexmath.TeXConstants;
import org.scilab.forge.jlatexmath.TeXFormula;
import org.scilab.forge.jlatexmath.TeXIcon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Модуль 7.3
 */

public class TaskModule_7_3 implements ITaskModule {

    boolean descriptionInitialized;
    boolean solutionInitialized;
    boolean inputInitialized;
    private JPanel descriptionPanel;
    private JLabel taskDescription;
    private JPanel solutionPanel;
    private ArrayList<JLabel> solutionLabels;
    private JPanel inputPanel;

    private JTextField variance0Field;
    private JTextField variance1Field;
    private JTextField mean0Field;
    private JTextField mean1Field;
    private JTextField c00;
    private JTextField c01;
    private JTextField c10;
    private JTextField c11;
    private JTextField p0;
    private JTextField p1;
    private JTextField zInput;

    private Chart2D inputChart;
    private ITrace2D func1;
    private ITrace2D func2;
    private RangePolicyFixedViewport xAxisRangePolicy;
    private RangePolicyFixedViewport yAxisRangePolicy;
    private Range xAxisRange;
    private Range yAxisRange;

    private double variance0;
    private double variance1;
    private double mean0;
    private double mean1;

    public ActionListener defaultValuesListener;
    public ActionListener saveValuesListener;
    public ActionListener loadValuesListener;

    public TaskModule_7_3() {
        defaultValuesListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                variance0Field.setText("0.7");
                variance1Field.setText("0.9");
                mean0Field.setText("1.0");
                mean1Field.setText("-1.0");
                c00.setText("0");
                c01.setText("0.4");
                c10.setText("0.6");
                c11.setText("0");
                p0.setText("0.5");
                p1.setText("0.5");
                zInput.setText("0.89");
            }
        };
        saveValuesListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JFileChooser saveDialog = new JFileChooser();
    			saveDialog.showSaveDialog(inputPanel);
                File saveFile = saveDialog.getSelectedFile();
                try {
                    FileWriter writer = new FileWriter(saveFile);
                    writer.write(variance0Field.getText()+"\r\n");
                    writer.write(variance1Field.getText()+"\r\n");
                    writer.write(mean0Field.getText()+"\r\n");
                    writer.write(mean1Field.getText()+"\r\n");
                    writer.write(c00.getText()+"\r\n");
                    writer.write(c01.getText()+"\r\n");
                    writer.write(c10.getText()+"\r\n");
                    writer.write(c11.getText()+"\r\n");
                    writer.write(p0.getText()+"\r\n");
                    writer.write(p1.getText()+"\r\n");
                    writer.write(zInput.getText()+"\r\n");
                    writer.close();
                }  catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        loadValuesListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JFileChooser openDialog = new JFileChooser();
                openDialog.showOpenDialog(inputPanel);
                File openFile = openDialog.getSelectedFile();
                try {
                    Scanner scanner = new Scanner(openFile);
                    variance0Field.setText(scanner.nextLine());
                    variance1Field.setText(scanner.nextLine());
                    mean0Field.setText(scanner.nextLine());
                    mean1Field.setText(scanner.nextLine());
                    c00.setText(scanner.nextLine());
                    c01.setText(scanner.nextLine());
                    c10.setText(scanner.nextLine());
                    c11.setText(scanner.nextLine());
                    p0.setText(scanner.nextLine());
                    p1.setText(scanner.nextLine());
                    zInput.setText(scanner.nextLine());
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }



            }
        };

        //Инициализируем все остальное по ходу дела
        descriptionInitialized = false;
        solutionInitialized = false;
        inputInitialized = false;
    }

    public void initDescriptionObjects() {
        descriptionPanel = new JPanel();
        Dimension dim = new Dimension(730, 300);
        descriptionPanel.setMaximumSize(dim);
        descriptionPanel.setSize(dim);
        descriptionPanel.setMinimumSize(dim);
        descriptionPanel.setPreferredSize(dim);
        descriptionPanel.setBackground(Color.white);
        descriptionPanel.setBorder(BorderFactory.createEtchedBorder());
        taskDescription = new JLabel(
                "<html>" +
                        "<style type=\"text/css\">\n" +
                        "   .myblock { \n" +
                        "    width: 550px; \n" +
                        "    background: #ffffff;\n" +
                        "    padding: 20px;\n" +
                        "    float: left;\n" +
                        "   }\n" +
                        "</style> \n" +
                        "<div class=myblock>Рассмотрим задачу обнаружения подвижного объекта с использованием радиолокационных средств.\n" +
                        "По результатам получаемых измерений необходимо определить наличие объекта или подтвердить его отсутствие\n" +
                        "в контролируемом пространстве. Множество возможных состояний контролируемого пространства <b>X = { x1 , x0 }</b> , где <b>x1</b> означает, что объект находится в заданном\n" +
                        "пространстве, а <b>x0</b> означает, что объекта нет. Обозначим <b>Г1</b> гипотезу о нахождении \n" +
                        "пространства в состоянии <b>x1</b> и <b>Г0</b> – гипотезу о нахождении пространства в состоянии <b>x0</b>. Вероятность нахождения пространства в состоянии x 1 и,\n" +
                        "соответственно, вероятность гипотезы <b>Г1</b> равна <b>p1 = Pr{ x = x1 } = Pr{ Г1 }.</b>\n" +
                        "Вероятность нахождения пространства в состоянии <b>x0</b> и, соответственно, вероятность гипотезы <b>Г0</b>\n" +
                        "равна <b>p0 = Pr{ x = x 0 } = Pr{ Г0 }</b>.</div></html>");
        descriptionPanel.add(taskDescription);
        descriptionInitialized = true;
    }

    public void initSolutionObjects() {
        //Hardcode everything!
        solutionPanel = new JPanel();

        solutionPanel.setLayout(new FlowLayout());
        Dimension dim = new Dimension(750, 1500);
        solutionPanel.setMaximumSize(dim);
        solutionPanel.setSize(dim);
        solutionPanel.setMinimumSize(dim);
        solutionPanel.setPreferredSize(dim);
        solutionPanel.setBorder(BorderFactory.createEtchedBorder());
        solutionPanel.setBackground(Color.white);
        solutionLabels = new ArrayList<>(30);

        solutionLabels.add(new JLabel("Решающее правило"));
        solutionLabels.add(printLatex("d\\in D_{z}"));
        solutionLabels.add(new JLabel("определяет разбиение"));
        solutionLabels.add(printLatex("Z = Z_{0}\\cup Z_{1}"));
        solutionLabels.add(printLatex("Z_{0}\\cap Z_{1} = \\O"));
        solutionLabels.add(printLatex("d(z) = \\left\\{\\begin{matrix}d_{1},z\\in Z_{1}\\\\ d_{0},z\\in Z_{0}\\end{matrix}\\right."));
        solutionLabels.add(new JLabel("<html>Решение d1 – решение включить сирену и оповестить о наличии объекта,<br> решение d0 – не производить никаких действий.</html>"));
        solutionLabels.add(new JLabel("<html>С применением любого решающего правила связана возможность принятия ошибочного решения.</html>"));
        solutionLabels.add(new JLabel("<html>Условные риски от принятия решающего правила d равны:</html>"));
        solutionLabels.add(printLatex("R(x_{1},d)\\int_{Z} c(x_{1},d(z))f(z/x_{1})dz=c_{11}(1-\\beta)+c{10}\\beta;"));
        solutionLabels.add(printLatex("R(x_{0},d)\\int_{Z} c(x_{0},d(z))f(z/x_{0})dz=c_{01}(1-\\alpha)+c_{00}\\alpha;"));
        solutionLabels.add(new JLabel("<html>Средний риск принятия решающего правила d равен:</html>"));
        solutionLabels.add(printLatex("R(d)=p_{1}[c_{11}(1-\\beta)+c_{10}\\beta]+p_{0}[c_{01}(1-\\alpha)+c_{00}\\alpha]."));
        solutionLabels.add(new JLabel("<html>Апостериорный риск принятия решающего правила d равен:</html>"));
        solutionLabels.add(printLatex("\\tilde{R}(d_{0}/z)=c_{11}Pr\\left\\{x_{1}/z\\right\\}+c_{01}Pr\\left\\{x_{0}/z \\right\\}=c_{11}Pr\\left\\{x_{1}/z\\right\\}+c_{01}Pr\\left\\{x_{0}/z \\right\\};"));
        solutionLabels.add(printLatex("\\tilde{R}(d_{0}/z)=c_{10}Pr\\left\\{x_{1}/z\\right\\}+c_{00}Pr\\left\\{x_{0}/z \\right\\}=c_{10}Pr\\left\\{x_{1}/z\\right\\}+c_{00}Pr\\left\\{x_{0}/z \\right\\}."));
        solutionLabels.add(new JLabel("Байесовское решающее правило"));
        solutionLabels.add(printLatex("d\\ast =\\left\\{\\begin{matrix} d_{1}, \\text{if} &   R(d_{0},z)\\geq \\tilde{R}(d_{1},z)\\\\ d_{0},  \\text{if} & R(d_{0},z)<\\tilde{R}(d_{1},z)  \\end{matrix}\\right."));
        solutionLabels.add(new JLabel("Рассмотрим случай, когда d*=d1, тогда "));
        solutionLabels.add(printLatex("\\tilde{R}(d_{0},z)\\geq \\tilde{R}(d_{1},z)."));
        solutionLabels.add(new JLabel("Выполним ряд преобразований:"));
        solutionLabels.add(printLatex("(c_{10}-c_{11})Pr\\left \\{ x_{1}/z \\right \\}-(c_{01}-c_{00})Pr\\left \\{ x_{0}/z \\right \\}\\geq 0;\\frac{Pr\\left \\{ x_{1}/z \\right \\}}{Pr\\left \\{ x_{0}/z \\right \\}}\\geq \\frac{c_{01}-c_{00}}{c_{10}-c_{11}}."));
        solutionLabels.add(new JLabel("С учетом того, что "));
        solutionLabels.add(printLatex("Pr\\left \\{ x_{i}/z \\right \\}=\\frac{p_{i}f(z/x_{i})}{f(z)},"));
        solutionLabels.add(new JLabel("получаем:"));
        solutionLabels.add(printLatex("\\frac{f(z/x_{1})}{f(z/x_{0})}\\geq \\frac{p_{0}(c_{01}-c_{00})}{p_{1}(c_{10}-c_{11})}"));
        solutionLabels.add(new JLabel("Тогда "));
        solutionLabels.add(printLatex("d\\ast =\\left\\{\\begin{matrix}d_{1}, \\text{if}& l(z)\\geq  \\Delta; \\\\ d_{0}, \\text{if}&l(z)<  \\Delta, & \\end{matrix}\\right."));
        solutionLabels.add(new JLabel("где "));
        solutionLabels.add(printLatex("l(z)=\\frac{f(z/x_{1})}{f(z/x_{0})}"));
        solutionLabels.add(new JLabel("– отношение правдоподобия;"));
        solutionLabels.add(printLatex("\\Delta =\\frac{p_{0}(c_{01}-c_{00})}{p_{1}(c_{10}-c_{11})}"));
        solutionLabels.add(new JLabel("– пороговое значение. При равных вероятностях p0=p1 обычно"));
        solutionLabels.add(printLatex("(c_{01}-c_{00})\\gg (c_{10}-c_{11}) "));
        solutionLabels.add(new JLabel("и тогда"));
        solutionLabels.add(printLatex("\\Delta \\ll 1"));
        solutionLabels.add(new JLabel("<html>Пусть задана функция правдоподобия f(z/x) , вероятности нахождения пространства в различных состояниях <br>одинаковые p0=p1=1/2, пороговое значение Δ= 0.2 .<html>"));
        solutionLabels.add(new JLabel("Если наблюдения имеют нормальное распределение, т. е."));
        solutionLabels.add(printLatex("f(z/x_{0})=N(z-\\Theta _{0},\\delta _{0}^{2})=\\frac{1}{\\delta _{0}\\sqrt{2\\pi }}e^{\\frac{(z-\\Theta _{0})^{2}}{2\\delta _{0}^{2}}}; f(z/x_{1})= N(z-\\Theta _{1},\\delta _{1}^{2})=\\frac{1}{\\delta _{1}\\sqrt{2\\pi }}e^{\\frac{(z-\\Theta _{1})^{2}}{2\\delta _{1}^{2}}}"));
        solutionLabels.add(new JLabel("тогда отношение правдоподобия имеет вид"));
        solutionLabels.add(printLatex("l(z)=\\frac{\\sigma _{0}}{\\sigma _{1}}\\text{exp}\\left \\{ \\frac{(z-\\Theta _{0})^{2}}{2\\sigma _{0}^{2}}-\\frac{(z-\\Theta _{1})^{2}}{2\\sigma _{1}^{2}} \\right \\}."));
        solutionLabels.add(new JLabel("Для удобства используется логарифм отношения правдоподобия:"));
        solutionLabels.add(printLatex("\\Lambda =\\text{ln} l(z)=\\text{ln}\\frac{\\sigma _{0}}{\\sigma _{1}}+\\frac{(z-\\Theta _{0})^{2}}{2\\sigma _{0}^{2}}-\\frac{(z-\\Theta _{1})^{2}}{2\\sigma _{1}^{2}}."));
        solutionLabels.add(new JLabel("Тогда байесовское решающее правило имеет вид:"));
        solutionLabels.add(printLatex("d\\ast =\\left\\{\\begin{matrix} d_{1}, \\text {if} &\\Lambda (z)\\geq \\text{ln}\\Delta  \\\\  d_{0}, \\text {if} & \\Lambda (z)<  \\text{ln}\\Delta  \\end{matrix}\\right.\n"));

        for (JLabel label : solutionLabels)
            solutionPanel.add(label);
        solutionInitialized = true;
    }

    //Normal distribution PDF
    public double normal(double x, double sigma, double theta) {
        return (1.0 / (Math.sqrt(sigma * 2.0 * Math.PI))) * (Math.exp(-Math.pow(x - theta, 2.0) / (2.0 * sigma)));
    }

    double ParseDouble(String strNumber) {
        if (strNumber != null && strNumber.length() > 0) {
            try {
                return Double.parseDouble(strNumber);
            } catch (Exception e) {
                return -1;
            }
        } else return 0;
    }

    public void redrawChart(Chart2D chart) {
        variance0 = ParseDouble(variance0Field.getText());
        variance1 = ParseDouble(variance1Field.getText());
        mean0 = ParseDouble(mean0Field.getText());
        mean1 = ParseDouble(mean1Field.getText());
        func1.removeAllPoints();
        func2.removeAllPoints();

        xAxisRange.setMax(4.0 * Math.max(Math.abs(mean0), Math.abs(mean1)));
        xAxisRange.setMin(-4.0 * Math.max(Math.abs(mean0), Math.abs(mean1)));
        yAxisRange.setMax(Math.max(normal(mean0, variance0, mean0), normal(mean1, variance1, mean1)));
        yAxisRange.setMin(0);

        IAxis<IAxisScalePolicy> xAxis = (IAxis<IAxisScalePolicy>) chart.getAxisX();
        IAxis<IAxisScalePolicy> yAxis = (IAxis<IAxisScalePolicy>) chart.getAxisY();
        xAxis.setRangePolicy(xAxisRangePolicy);
        yAxis.setRangePolicy(yAxisRangePolicy);
        xAxis.setMinorTickSpacing(xAxisRange.getMax() / 5);
        yAxis.setMinorTickSpacing(yAxisRange.getMax() / 10);

        for (double x = xAxisRange.getMin(); x < xAxisRange.getMax(); x += 0.01) {
            double val = normal(x, variance0, mean0);
            double val2 = normal(x, variance1, mean1);
            func1.addPoint(x, val);
            func2.addPoint(x, val2);
        }
    }

    public void initInputObjects() {
        inputPanel = new JPanel();
        variance0Field = new JTextField();
        variance1Field = new JTextField();
        variance0Field.setColumns(4);
        variance1Field.setColumns(4);
        mean0Field = new JFormattedTextField();
        mean1Field = new JFormattedTextField();
        mean0Field.setColumns(4);
        mean1Field.setColumns(4);
        c00 = new JFormattedTextField();
        c00.setColumns(4);
        c00.setMaximumSize(new Dimension(50, 25));
        c01 = new JFormattedTextField();
        c01.setColumns(4);
        c01.setMaximumSize(new Dimension(50, 25));
        c10 = new JFormattedTextField();
        c10.setColumns(4);
        c10.setMaximumSize(new Dimension(50, 25));
        c11 = new JFormattedTextField();
        c11.setColumns(4);
        c11.setMaximumSize(new Dimension(50, 25));
        p0 = new JFormattedTextField();
        p0.setColumns(4);
        p0.setMaximumSize(new Dimension(50, 25));
        p1 = new JFormattedTextField();
        p1.setColumns(4);
        p1.setMaximumSize(new Dimension(50, 25));
        zInput = new JFormattedTextField();
        zInput.setColumns(4);
        zInput.setMaximumSize(new Dimension(60, 25));

        inputPanel.setLayout(new BoxLayout(inputPanel, BoxLayout.LINE_AXIS));
        Dimension dim = new Dimension(750, 380);
        inputPanel.setBackground(Color.WHITE);
        inputPanel.setMaximumSize(dim);
        inputPanel.setSize(dim);
        inputPanel.setMinimumSize(dim);
        inputPanel.setPreferredSize(dim);
        inputPanel.setBorder(BorderFactory.createEtchedBorder());

        JPanel normalParams0 = new JPanel();
        JPanel normalParams1 = new JPanel();
        normalParams0.setBackground(Color.WHITE);
        normalParams1.setBackground(Color.WHITE);
        normalParams0.setMaximumSize(new Dimension(320, 25));
        normalParams1.setMaximumSize(new Dimension(320, 25));
        normalParams0.setLayout(new BoxLayout(normalParams0, BoxLayout.LINE_AXIS));
        normalParams1.setLayout(new BoxLayout(normalParams1, BoxLayout.LINE_AXIS));

        normalParams0.add(printLatex("\\sigma^{2}_{0}="));
        variance0Field.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                redrawChart(inputChart);
            }
        });
        normalParams0.add(variance0Field);
        normalParams1.add(printLatex("\\sigma^{2}_{1}="));
        variance1Field.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                redrawChart(inputChart);
            }
        });
        normalParams1.add(variance1Field);
        normalParams0.add(printLatex("\\theta_{0}="));
        mean0Field.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                redrawChart(inputChart);
            }
        });
        normalParams0.add(mean0Field);
        normalParams1.add(printLatex("\\theta_{1}="));
        mean1Field.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                redrawChart(inputChart);
            }
        });
        normalParams1.add(mean1Field);

        inputChart = new Chart2D();
        Dimension chartSize = new Dimension(400, 350);
        inputChart.setMaximumSize(chartSize);
        inputChart.setMinimumSize(chartSize);
        inputChart.setSize(chartSize);
        func1 = new Trace2DSimple();
        func2 = new Trace2DSimple();
        func1.setName("f(z/x0)");
        func2.setName("f(z/x1)");
        Stroke stroke1 = new BasicStroke(1);
        func1.setStroke(stroke1);
        func2.setStroke(stroke1);
        func1.setColor(Color.RED);
        func2.setColor(Color.BLUE);
        inputChart.addTrace(func1);
        inputChart.addTrace(func2);
        xAxisRange = new Range(-3, 3);
        yAxisRange = new Range(0, 1);
        xAxisRangePolicy = new RangePolicyFixedViewport(xAxisRange);
        yAxisRangePolicy = new RangePolicyFixedViewport(yAxisRange);
        IAxis<IAxisScalePolicy> xAxis = (IAxis<IAxisScalePolicy>) inputChart.getAxisX();
        xAxis.setRangePolicy(xAxisRangePolicy);
        xAxis.setAxisScalePolicy(new AxisScalePolicyManualTicks());
        xAxis.setMinorTickSpacing(0.5);
        xAxis.setAxisTitle(new IAxis.AxisTitle("z"));
        IAxis<IAxisScalePolicy> yAxis = (IAxis<IAxisScalePolicy>) inputChart.getAxisY();
        yAxis.setRangePolicy(yAxisRangePolicy);
        yAxis.setAxisScalePolicy(new AxisScalePolicyManualTicks());
        yAxis.setMinorTickSpacing(0.5);
        yAxis.setAxisTitle(new IAxis.AxisTitle("f ( z / x )"));


        JPanel chartPanel = new JPanel();
        chartPanel.setLayout(new BoxLayout(chartPanel, BoxLayout.PAGE_AXIS));
        Dimension chartDimension = new Dimension(400, 350);
        chartPanel.setMaximumSize(chartDimension);
        chartPanel.setSize(chartDimension);
        chartPanel.setMinimumSize(chartDimension);
        chartPanel.setPreferredSize(chartDimension);
        chartPanel.setBorder(BorderFactory.createEtchedBorder());
        chartPanel.add(inputChart);

        Container westContainer = new Container();
        westContainer.setLayout(new BoxLayout(westContainer, BoxLayout.Y_AXIS));

        Container buttonContainer = new Container();
        buttonContainer.setLayout(new BorderLayout());

        JButton redrawButton = new JButton("Перестроить график");
        redrawButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                redrawChart(inputChart);
            }
        });

        buttonContainer.add(redrawButton);

        westContainer.add(buttonContainer);
        westContainer.add(chartPanel);
        inputPanel.add(westContainer);

        JPanel eastContainer = new JPanel();
        eastContainer.setMinimumSize(new Dimension(350, 380));
        eastContainer.setMaximumSize(new Dimension(350, 380));
        eastContainer.setBackground(Color.WHITE);
        eastContainer.setLayout(new BoxLayout(eastContainer, BoxLayout.Y_AXIS));
        eastContainer.add(Box.createRigidArea(new Dimension(390, 25)));
        Container labelContainer = new Container();
        labelContainer.setLayout(new BoxLayout(labelContainer, BoxLayout.X_AXIS));
        labelContainer.add(new JLabel("Введите параметры нормального распределения:"));
        eastContainer.add(labelContainer);
        eastContainer.add(Box.createRigidArea(new Dimension(390, 10)));

        eastContainer.add(normalParams0);
        eastContainer.add(normalParams1);
        eastContainer.add(Box.createRigidArea(new Dimension(390, 25)));
        Container cRow1 = new Container();
        Container cRow2 = new Container();

        JLabel cCaption = new JLabel("Значения функции потерь:");
        Container captionContainer = new Container();
        captionContainer.setLayout(new BoxLayout(captionContainer, BoxLayout.X_AXIS));
        captionContainer.add(cCaption);

        eastContainer.add(captionContainer);
        cRow1.setLayout(new BoxLayout(cRow1, BoxLayout.X_AXIS));
        cRow2.setLayout(new BoxLayout(cRow2, BoxLayout.X_AXIS));
        JLabel c00label = new JLabel("c00 (x0,d0) =");
        JLabel c01label = new JLabel("c01 (x0,d1) =");
        JLabel c10label = new JLabel("c10 (x1,d0) =");
        JLabel c11label = new JLabel("c11 (x1,d1) =");
        cRow1.add(c00label);
        cRow1.add(Box.createRigidArea(new Dimension(10, 25)));
        cRow1.add(c00);
        cRow1.add(Box.createRigidArea(new Dimension(10, 25)));
        cRow1.add(c01label);
        cRow1.add(Box.createRigidArea(new Dimension(10, 25)));
        cRow1.add(c01);
        cRow2.add(c10label);
        cRow2.add(Box.createRigidArea(new Dimension(10, 25)));
        cRow2.add(c10);
        cRow2.add(Box.createRigidArea(new Dimension(10, 25)));
        cRow2.add(c11label);
        cRow2.add(Box.createRigidArea(new Dimension(10, 25)));
        cRow2.add(c11);

        eastContainer.add(Box.createRigidArea(new Dimension(400, 10)));
        eastContainer.add(cRow1);
        eastContainer.add(cRow2);
        eastContainer.add(Box.createRigidArea(new Dimension(400, 10)));

        Container p0Container = new Container();
        p0Container.setLayout(new BoxLayout(p0Container, BoxLayout.X_AXIS));
        p0Container.add(new JLabel("Вероятность состояния x0 (p0) = "));
        p0Container.add(p0);
        eastContainer.add(p0Container);
        eastContainer.add(Box.createRigidArea(new Dimension(400, 10)));
        Container p1Container = new Container();
        p1Container.setLayout(new BoxLayout(p1Container, BoxLayout.X_AXIS));
        p1Container.add(new JLabel("Вероятность состояния x1 (p1) = "));
        p1Container.add(p1);
        eastContainer.add(p1Container);
        eastContainer.add(Box.createRigidArea(new Dimension(400, 10)));
        Container zRow = new Container();
        zRow.setMinimumSize(new Dimension(400, 25));
        zRow.setLayout(new BoxLayout(zRow, BoxLayout.X_AXIS));
        zRow.add(new JLabel("Наблюдение Z = "));
        zRow.add(zInput);
        eastContainer.add(zRow);

        inputPanel.add(eastContainer);
        inputInitialized = true;
    }

    @Override
    public String getTitle() {
        return "7.3 Байесовское\nрешающее правило";
    }


    public JLabel printLatex(String latex) {
        TeXFormula formula = new TeXFormula(latex);
        TeXIcon icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, 20);
        icon.setInsets(new Insets(5, 5, 5, 5));
        BufferedImage image = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = image.createGraphics();
        g2.setColor(Color.white);
        g2.fillRect(0, 0, icon.getIconWidth(), icon.getIconHeight());
        JLabel jl = new JLabel();
        jl.setForeground(new Color(0, 0, 0));
        icon.paintIcon(jl, g2, 0, 0);
        JLabel formulaIcon = new JLabel(new ImageIcon(image));
        formulaIcon.setBounds(20, 20, 200, 100);
        formulaIcon.setHorizontalAlignment(JLabel.CENTER);
        return formulaIcon;
    }

    public double calcLambda(double z) {
        return Math.log(Math.sqrt(variance0) / Math.sqrt(variance1)) + Math.pow(z - mean0, 2.0) / (2 * variance0) - Math.pow(z - mean1, 2.0) / (2 * variance1);
    }

    public double calcDelta(double p0, double p1, double c00, double c01, double c10, double c11) {
        return (p0 * (c01 - c00)) / (p1 * (c10 - c11));
    }

    public JPanel runCalculations() {
        redrawChart(inputChart);
        JPanel examplePanel = new JPanel();
        examplePanel.setLayout(new BoxLayout(examplePanel, BoxLayout.PAGE_AXIS));
        examplePanel.setBackground(Color.WHITE);
        Dimension dim = new Dimension(800, 1300);
        examplePanel.setMaximumSize(dim);
        examplePanel.setSize(dim);
        examplePanel.setMinimumSize(dim);
        examplePanel.setPreferredSize(dim);
        examplePanel.setBorder(BorderFactory.createEtchedBorder());
        examplePanel.setBackground(Color.white);

        double p0value = ParseDouble(p0.getText());
        double p1value = ParseDouble(p1.getText());

        double c00value = ParseDouble(c00.getText());
        double c01value = ParseDouble(c01.getText());
        double c10value = ParseDouble(c10.getText());
        double c11value = ParseDouble(c11.getText());

        double zvalue = ParseDouble(zInput.getText());

        Container pValues = new Container();
        pValues.setLayout(new BoxLayout(pValues, BoxLayout.X_AXIS));
        Container cValues = new Container();
        cValues.setLayout(new BoxLayout(cValues, BoxLayout.X_AXIS));

        pValues.add(printLatex("p_{0}=" + String.valueOf(p0value)));
        pValues.add(printLatex("p_{1}=" + String.valueOf(p1value)));
        cValues.add(printLatex("c_{00}=" + String.valueOf(c00value)));
        cValues.add(printLatex("c_{01}=" + String.valueOf(c01value)));
        cValues.add(printLatex("c_{10}=" + String.valueOf(c10value)));
        cValues.add(printLatex("c_{11}=" + String.valueOf(c11value)));

        Container zValues = new Container();
        zValues.setLayout(new BoxLayout(zValues, BoxLayout.X_AXIS));
        zValues.add(printLatex("z=" + String.valueOf(zvalue)));

        examplePanel.add(cValues);
        examplePanel.add(pValues);
        examplePanel.add(zValues);

        Container normalValues = new Container();
        normalValues.setLayout(new BoxLayout(normalValues, BoxLayout.X_AXIS));
        normalValues.add(printLatex("\\sigma{_{0}}^{2} = " + String.valueOf(variance0)));
        normalValues.add(printLatex("\\theta_{0} = " + String.valueOf(mean0)));
        normalValues.add(printLatex("\\sigma{_{1}}^{2} = " + String.valueOf(variance1)));
        normalValues.add(printLatex("\\theta_{1} = " + String.valueOf(mean1)));

        examplePanel.add(normalValues);

        Chart2D exampleChart = new Chart2D();
        exampleChart.addTrace(func1);
        exampleChart.addTrace(func2);

        Trace2DSimple zTrace = new Trace2DSimple();
        zTrace.setColor(Color.orange);
        zTrace.setName("Z");
        exampleChart.addTrace(zTrace);

        for (int i = 0; i < 10; i++) {
            zTrace.addPoint(zvalue, i * 10);
        }

        redrawChart(exampleChart);

        JPanel chartPanel = new JPanel();
        chartPanel.setLayout(new BorderLayout());
        chartPanel.setMaximumSize(new Dimension(500, 400));
        chartPanel.add(exampleChart);
        examplePanel.add(chartPanel);

        Container lambdaCalc = new Container();
        lambdaCalc.setLayout(new BoxLayout(lambdaCalc, BoxLayout.X_AXIS));
        lambdaCalc.add(printLatex("\\Lambda =\\text{ln} l(z)=\\text{ln}\\frac{\\sigma _{0}}{\\sigma _{1}}+\\frac{(z-\\Theta _{0})^{2}}{2\\sigma _{0}^{2}}-\\frac{(z-\\Theta _{1})^{2}}{2\\sigma _{1}^{2}} = "));
        examplePanel.add(lambdaCalc);
        Container lambdaCalc2 = new Container();
        lambdaCalc2.setLayout(new BoxLayout(lambdaCalc2, BoxLayout.X_AXIS));
        double lambda = calcLambda(zvalue);
        lambdaCalc2.add(printLatex("=\\text{ln}\\frac{\\sqrt{" + String.valueOf(variance0) + "}}{\\sqrt{" + String.valueOf(variance1) + "}}+\\frac{(" + String.valueOf(zvalue) + "-" + String.valueOf(mean0) + ")^{2}}{2*" + String.valueOf(variance0) + "}-\\frac{(" + String.valueOf(zvalue) + "-" + String.valueOf(mean1) + ")^{2}}{2*" + String.valueOf(variance1) + "} = " + String.valueOf(lambda)));
        examplePanel.add(lambdaCalc2);

        Container deltaContainer = new Container();
        deltaContainer.setLayout(new BoxLayout(deltaContainer, BoxLayout.X_AXIS));
        double delta = calcDelta(p0value, p1value, c00value, c01value, c10value, c11value);

        deltaContainer.add(printLatex("\\Delta =\\frac{p_{0}(c_{01}-c_{00})}{p_{1}(c_{10}-c_{11})} = \\frac{" + String.valueOf(p0value) + "(" + String.valueOf(c01value) + "-" + String.valueOf(c00value) + ")}{" + String.valueOf(p1value) + "(" + String.valueOf(c10value) + "-" + String.valueOf(c11value) + ")} =" + String.valueOf(delta)));

        examplePanel.add(deltaContainer);

        Container decisionContainer = new Container();
        decisionContainer.setLayout(new BoxLayout(decisionContainer, BoxLayout.X_AXIS));
        decisionContainer.add(printLatex("d\\ast =\\left\\{\\begin{matrix} d_{1}, \\text {если} &\\Lambda (z)\\geq \\text{ln}\\Delta  \\\\  d_{0}, \\text {если} & \\Lambda (z)<  \\text{ln}\\Delta  \\end{matrix}\\right.\n"));
        decisionContainer.add(printLatex("\\Lambda = " + String.valueOf(lambda)));
        double lnDelta = Math.log(delta);
        decisionContainer.add(printLatex("\\ln \\Delta= " + String.valueOf(lnDelta)));
        examplePanel.add(decisionContainer);
        Container finalDecision = new Container();
        finalDecision.setLayout(new BoxLayout(finalDecision, BoxLayout.X_AXIS));
        if (lambda >= lnDelta) {
            finalDecision.add(printLatex("Решение\\:d = d1"));
        } else {
            finalDecision.add(printLatex("Решение\\: d = d0"));
        }
        examplePanel.add(finalDecision);

        return examplePanel;
    }

    @Override
    public void initDescriptionPanel(JPanel panel) {
        if (!descriptionInitialized) {
            initDescriptionObjects();
            panel.add(descriptionPanel);
        } else {
            panel.add(descriptionPanel);
        }
    }

    @Override
    public void initSolutionPanel(JPanel panel) {
        if (!solutionInitialized) {
            initSolutionObjects();
            panel.add(solutionPanel);
        } else {
            panel.add(solutionPanel);
        }
    }

    @Override
    public void initInputPanel(JPanel panel) {
        if (!inputInitialized) {
            initInputObjects();
            panel.add(inputPanel);
        } else {
            panel.add(inputPanel);
        }
    }

    @Override
    public void initExamplePanel(JPanel panel) {
        panel.removeAll();
        panel.add(runCalculations());
    }

    @Override
    public ActionListener getPressSaveListener() throws IllegalArgumentException {
        return saveValuesListener;
    }

    @Override
    public ActionListener getPressLoadListener() throws IllegalArgumentException {
        return loadValuesListener;
    }

    @Override
    public ActionListener getDefaultValuesListener() throws IllegalArgumentException {
        return defaultValuesListener;
    }
}

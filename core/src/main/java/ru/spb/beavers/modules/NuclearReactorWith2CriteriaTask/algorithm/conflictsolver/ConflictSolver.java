package ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.algorithm.conflictsolver;

import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility.ListenerHandler;
import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility.listener.IStepLogger;

public abstract class ConflictSolver extends ListenerHandler<IStepLogger> implements IConflictSolver  {
    abstract public String getTypeString();

    protected void logAction(String str){
        for (IStepLogger handler:getHandlersList()){
            handler.logStep(str);
        }
    }
}
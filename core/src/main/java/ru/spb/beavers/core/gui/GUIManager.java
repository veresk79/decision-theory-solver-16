package ru.spb.beavers.core.gui;

import javax.swing.*;
import java.awt.*;

/**
 * Менеджер View
 */
public class GUIManager {

    private static JPanel CONTENT_PANE = new JPanel(new GridLayout(1,1));
    private static MenuView MENU_VIEW = new MenuView();
    private static TheoryView THEORY_VIEW = new TheoryView();
    private static InputView INPUT_VIEW = new InputView();
    private static DescriptionView DESCRIPTION_VIEW = new DescriptionView();
    private static ExampleView EXAMPLE_VIEW = new ExampleView();

    static {
        CONTENT_PANE.add(MENU_VIEW);
    }

    public static JPanel getContentPane() {
        return CONTENT_PANE;
    }

    public static MenuView getMenuView() {
        return MENU_VIEW;
    }

    public static TheoryView getTheoryView() {
        return THEORY_VIEW;
    }

    public static InputView getInputView() {
        return INPUT_VIEW;
    }

    public static DescriptionView getDescriptionView() {
        return DESCRIPTION_VIEW;
    }

    public static ExampleView getExampleView() {
        return EXAMPLE_VIEW;
    }

    public static void setActiveView(JPanel view) {
        CONTENT_PANE.removeAll();
        CONTENT_PANE.add(view);
        CONTENT_PANE.updateUI();
    }
}
